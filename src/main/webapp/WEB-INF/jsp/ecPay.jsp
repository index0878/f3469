<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/contactStyle.css">
    <title>FARM CAPYZOU</title>
  </head>
  <body>
    <header>
      <h1><img src="images/logo.png" alt="FARM CAPYZOU"></h1>
      <nav>
        <ul>
          <li><a href="home">HOME</a></li>
          <li><a href="home">ABOUT</a></li>
          <li><a href="blog">BLOG</a></li>
          <li><a href="shop">SHOP</a></li>
          <li><a href="contact">CONTACT</a></li>
        </ul>
      </nav>
    </header>
    <div>
      <main>
        <article>
          <section class="formSec" id="formArea">
              <h2 class="ffJosefin">ECPAY</h2>
              <div id="formtext">${formtext}</div>
          </section>
        </article>
      </main>
    </div>
    <footer>
      <p><small>© 3021 FARM CAPYZOU</small></p>
    </footer>
  </body>
</html>