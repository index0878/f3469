<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="zh-Hant-TW">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/homeStyle.css">
    <title>水豚藏自我介紹</title>
  </head>
  <body>
<!--     <h1>水豚藏</h1> -->
    <h1>${vo.name}</h1>
    <p>
      <img src="data:image/jpg;base64,${vo.logo}" alt="水豚藏的大頭照">
<!--       <img src="capyzou.png" alt="水豚藏的大頭照"> -->
    </p>
    <p>
<!--       我經營了一座農場。<br> -->
<!--       歡迎大家追蹤關注我！ -->
		${vo.text}
    </p>
    <ul><%=request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/blog" %>
<!--     	<li><a href='/blog'>Blog</a></li> -->
    	<li><a href='blog'>Blog</a></li>
    <c:forEach items="${vo.homeLinks}" var="link">    
    	<li><a href="${link.link }">${link.title }</a></li>
    </c:forEach>
<!--       <li><a href="#">Capitter</a></li> -->
<!--       <li><a href="#">Capistagram</a></li> -->
<!--       <li><a href="#">Capybook</a></li> -->
    </ul>
  </body>
</html>