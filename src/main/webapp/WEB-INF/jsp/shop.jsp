<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/shopStyle.css">
    <title>FARM CAPYZOU</title>
  </head>
  <body>
    <header>
      <h1><img src="images/logo.png" alt="FARM CAPYZOU"></h1>
      <nav>
        <ul>
          <li><a href="home">HOME</a></li>
          <li><a href="home">ABOUT</a></li>
          <li><a href="blog">BLOG</a></li>
          <li><a href="shop">SHOP</a></li>
          <li><a href="contact">CONTACT</a></li>
        </ul>
      </nav>
    </header>
    <div>
      <main>
        <article>
          <section class="formSec">
            <h2 class="ffJosefin">商品列表</h2>
            <form method="post" action="buyByECPay" id="buyForm">
            <c:forEach items="${vo.shops }" var="shop" varStatus="status">
      			<img src="data:image/jpg;base64,${shop.blob}" >
                <h3>${shop.name }</h3>
                <p>${shop.text }</p>
                <p>價格: NT$ ${shop.price}</p>
                <input type="hidden" name = "shops[${status.index}].code" value="${shop.code}">
                <p>購買數量<input type="number" value="0" id="${shop.code }" name="shops[${status.index}].count" min="0" price="${shop.price }"></p>
            </c:forEach>
<!--                 <img src="images/h2_icon.png" alt="商品 2"> -->
<!--                 <h3>商品 2</h3> -->
<!--                 <p>商品描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述</p> -->
<!--                 <p>價格: NT$ 2000</p> -->
<!--                 <input type="number" value="0" id="shop2" name="count" min="0" price="2000"> -->
          <p>購買人姓名:<input type="text" id="buyName" name="name"></p>
          <p>寄件地址:<input type="text" id="addr"></p>
          <p>購買人email:<input type="email" id="email"></p>
          <p>購買人手機:<input type="tel" id="cellphone"></p>
          <h3>總金額：NT$ <input id="total" value=0></h3>
          <p class="submitBtn">
	          <input type="button" class="ffJosefin" id="buy" value="結帳">
          </p>
          <p class="submitBtn">
	          <input type="submit" class="ffJosefin"  value="ECPay">
          </p>
            </form>
          </section>
        </article>
      </main>
      <aside>
        <nav class="categoryNav">
          <h2>Category</h2>
          <ul>
          	<c:forEach items="${vo.categories}" var="category">
          		<li><a href="category?categoryid=${category.id}">${category.name}(${category.count})</a></li>
          	</c:forEach>
          </ul>
        </nav>
        <nav class="recentNav">
          <h2>Recent Articles</h2>
          <ul>
          	<c:forEach items="${vo.recentArticles }" var="recentArticles">
          		<li><a href="indexblog?blogid=${recentArticles.index}">${recentArticles.title}</a></li>
       		</c:forEach>
          </ul>
        </nav>
        <p>
        	<c:forEach items="${vo.ads}" var="ad">
          		<a href="${ad.link}"><img src="data:image/jpg;base64,${ad.blob}" alt="${ad.title}"></a>
          	</c:forEach>
        </p>
      </aside>
    </div>
    <footer>
      <p><small>© 3021 FARM CAPYZOU</small></p>
    </footer>
    <script type="text/javascript">
   		 
   		var shopcar = document.querySelectorAll('input[type="number"]');


	    document.addEventListener("DOMContentLoaded", function () {
    		shopcar.forEach(element => {
        		element.onchange = function () {
         		var total = 0;
          		shopcar.forEach(element01 => {
    	        	total = total + Number(element01.value) * Number(element01.getAttribute("price"));
        		});
//           		alert(total);
          		document.getElementById("total").value = total;
        		}
      		});
    		
    		document.getElementById("buy").onclick = function(){
    			buy();
    		}

    	})

    	function buy(){
    		if(document.getElementById("total").value == 0){
            	alert("結帳金額不得為0")
            	return;
          	}

			var data = {};
     		data.name = document.getElementById("buyName").value;
     		data.address =document.getElementById("addr").value;
     		data.email = document.getElementById("email").value;
     		data.cellphone = document.getElementById("cellphone").value;
     		var arr = new Array();
     		shopcar.forEach(element => {
       			var shop = {};
       			shop.code= element.getAttribute("id");
       			shop.count= element.value;
       			arr.push(shop);
     		})
     		data.shops = arr;
   			// 創建 XMLHttpRequest 物件
			var xhr = new XMLHttpRequest();

			// 設定請求方法和請求路徑
			xhr.open("POST", "shop/buy");

		    // 設定請求頭
		    xhr.setRequestHeader("Content-Type", "application/json");

		    // 發送請求
		    xhr.send(JSON.stringify(data));

		    // 處理回應
		    xhr.onload = function() {
		    var data = JSON.parse(xhr.responseText);
		      // 檢查回應是否成功
		      if (xhr.status === 200) {
		        // 取得回應資料
		    	  console.log(data);

		        // 更新網頁內容
		        
		      }
			   alert(data.message);
			   if(data.code == 1000){
				   document.forms["buyForm"].submit();
			   }
		    };
    }

    </script>
  </body>
</html>