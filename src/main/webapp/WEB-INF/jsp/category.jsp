<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/blogStyle.css">
    <title>FARM CAPYZOU</title>
  </head>
  <body>
    <header>
      <h1><img src="images/logo.png" alt="FARM CAPYZOU"></h1>
      <nav>
        <ul>
          <li><a href="home">HOME</a></li>
          <li><a href="home">ABOUT</a></li>
          <li><a href="blog">BLOG</a></li>
          <li><a href="shop">SHOP</a></li>
          <li><a href="contact">CONTACT</a></li>
        </ul>
      </nav>
    </header>
    <div>
      <main>
        <article>
          <p class="eyecatch">
            <img src="images/eyecatch.png" alt="水豚藏在農場的插圖">
          </p>
          	<c:forEach items="${vo.data}" var="data">
          		<section><a href="indexblog?blogid=${data.index}"><h3>${data.title}</h3></a></section>
          	</c:forEach>
        </article>
      </main>
      <aside>
        <nav class="categoryNav">
          <h2>Category</h2>
          <ul>
          	<c:forEach items="${vo.categories}" var="category">
          		<li><a href="category?categoryid=${category.id}">${category.name}(${category.count})</a></li>
          	</c:forEach>
          </ul>
        </nav>
        <nav class="recentNav">
          <h2>Recent Articles</h2>
          <ul>
          	<c:forEach items="${vo.recentArticles }" var="recentArticles">
          		<li><a href="indexblog?blogid=${recentArticles.index}">${recentArticles.title}</a></li>
       		</c:forEach>
<!--             <li><a href="#">溫室栽培的10大注意事項</a></li> -->
<!--             <li><a href="#">現任農夫傳授的驅蟲方法</a></li> -->
<!--             <li><a href="#">在家也能種植！盆栽蔬菜的栽培重點</a></li> -->
<!--             <li><a href="#">介紹簡單的胡蘿蔔料理！</a></li> -->
<!--             <li><a href="#">務農可以維生嗎？大家好奇的年收入大公開</a></li> -->
<!--             <li><a href="#">你好，水豚藏的自我介紹</a></li> -->
          </ul>
        </nav>
        <p>
        	<c:forEach items="${vo.ads}" var="ad">
          		<a href="${ad.link}"><img src="data:image/jpg;base64,${ad.blob}" alt="${ad.title}"></a>
          	</c:forEach>
<!--           <a href="#"><img src="images/side_banner.png" alt="線上購物banner"></a> -->
        </p>
      </aside>
    </div>
    <footer>
      <p><small>© 3021 FARM CAPYZOU</small></p>
    </footer>
  </body>
</html>