<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/contactStyle.css">
    <title>FARM CAPYZOU</title>
  </head>
  <body>
    <header>
      <h1><img src="images/logo.png" alt="FARM CAPYZOU"></h1>
      <nav>
        <ul>
          <li><a href="home">HOME</a></li>
          <li><a href="home">ABOUT</a></li>
          <li><a href="blog">BLOG</a></li>
          <li><a href="shop">SHOP</a></li>
          <li><a href="contact">CONTACT</a></li>
        </ul>
      </nav>
    </header>
    <div>
      <main>
        <article>
          <section class="formSec" id="formArea">
              <h2 class="ffJosefin">聯絡我們</h2>
              <form action="sendForm" method="post" name="contactForm" id="contactForm">
                <br>
                <p>
                  姓名<input type="text" name="name">
                </p>
                <p>
                  電子郵件<input type="email" name="email">
                </p>
                <p>
                  您的類型
                  <select name="userType">
                    <option value="草食動物">草食動物</option>
                    <option value="肉食動物">肉食動物</option>
                    <option value="人類">人類</option>
                  </select>
                </p>
                <p class="attendRadio">
                  問題類型<br><br>
                  <label><input type="radio" name="questionType" value="IC">產業合作</label>
                  <label><input type="radio" name="questionType" value="PP">種植問題</label>
                  <label><input type="radio" name="questionType" value="CK">料理相關</label>
                  <label><input type="radio" name="questionType" value="OT">其他</label>
                </p>
                <p>
                  留言<br>
                  <textarea name="message"></textarea>
                </p>
                <p class="submitBtn">
                  <input type="button" id="contactBtn" class="ffJosefin" value="Send">
                </p>
              </form>
          </section>
        </article>
      </main>
      <aside>
        <nav class="categoryNav">
          <h2>Category</h2>
          <ul>
          	<c:forEach items="${vo.categories}" var="category">
          		<li><a href="category?categoryid=${category.id}">${category.name}(${category.count})</a></li>
          	</c:forEach>
          </ul>
        </nav>
        <nav class="recentNav">
          <h2>Recent Articles</h2>
          <ul>
          	<c:forEach items="${vo.recentArticles }" var="recentArticles">
          		<li><a href="indexblog?blogid=${recentArticles.index}">${recentArticles.title}</a></li>
       		</c:forEach>
          </ul>
        </nav>
        <p>
        	<c:forEach items="${vo.ads}" var="ad">
          		<a href="${ad.link}"><img src="data:image/jpg;base64,${ad.blob}" alt="${ad.title}"></a>
          	</c:forEach>
        </p>
      </aside>
    </div>
    <footer>
      <p><small>© 3021 FARM CAPYZOU</small></p>
    </footer>
    <script type="text/javascript">
   		 document.addEventListener("DOMContentLoaded", function () {
//    			document.getElementById("contactBtn").onclick=function(){
//    				alert('123')
//    			}
   			 document.getElementById("contactBtn").onclick=function(){
   				 alert('click')
   				// 創建 XMLHttpRequest 物件
   				    var xhr = new XMLHttpRequest();

   				    // 設定請求方法和請求路徑
   				    xhr.open("POST", "contact/sendForm");

   				    // 設定請求頭
//    				    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
   				    
   				    //
   					const formData = new FormData(document.getElementById("contactForm"));


   				    // 發送請求
   				    xhr.send(formData);

   				    // 處理回應
   				    xhr.onload = function() {
   				    var data = JSON.parse(xhr.responseText);
   				      // 檢查回應是否成功
   				      if (xhr.status === 200) {
   				        // 取得回應資料
   				    	  console.log(data);

   				        // 更新網頁內容
   				        
   				      }
   					   alert(data.message);
   				    };
   			 };
//    			document.getElementByName("name").onChange=function(){
//    				console.log(document.getElementByName("name").value())		
//    			}
   		 })

    </script>
  </body>
</html>