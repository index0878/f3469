<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/shopStyle.css">
    <title>FARM CAPYZOU</title>
  </head>
  <body>
    <header>
      <h1><img src="images/logo.png" alt="FARM CAPYZOU"></h1>
      <nav>
        <ul>
          <li><a href="home">HOME</a></li>
          <li><a href="home">ABOUT</a></li>
          <li><a href="blog">BLOG</a></li>
          <li><a href="shop">SHOP</a></li>
          <li><a href="contact">CONTACT</a></li>
        </ul>
      </nav>
    </header>
    <div>
      <main>
        <article>
          <section class="formSec">
            <h2 class="ffJosefin">感謝您的購買</h2>
          </section>
        </article>
      </main>
      <aside>
        <nav class="categoryNav">
          <h2>Category</h2>
          <ul>
          	<c:forEach items="${vo.categories}" var="category">
          		<li><a href="category?categoryid=${category.id}">${category.name}(${category.count})</a></li>
          	</c:forEach>
          </ul>
        </nav>
        <nav class="recentNav">
          <h2>Recent Articles</h2>
          <ul>
          	<c:forEach items="${vo.recentArticles }" var="recentArticles">
          		<li><a href="indexblog?blogid=${recentArticles.index}">${recentArticles.title}</a></li>
       		</c:forEach>
          </ul>
        </nav>
        <p>
        	<c:forEach items="${vo.ads}" var="ad">
          		<a href="${ad.link}"><img src="data:image/jpg;base64,${ad.blob}" alt="${ad.title}"></a>
          	</c:forEach>
        </p>
      </aside>
    </div>
    <footer>
      <p><small>© 3021 FARM CAPYZOU</small></p>
    </footer>
    <script type="text/javascript">

    </script>
  </body>
</html>