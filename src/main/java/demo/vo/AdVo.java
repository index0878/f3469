package demo.vo;

import lombok.Data;

@Data
public class AdVo {
	private String blob;
	private String Link;
	private String title;
}
