package demo.vo;

import java.util.List;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ShopVo {
	@NotBlank(message="姓名不可為空白")
	private String name;
	@NotBlank(message="地址不可為空白")
	private String address;
	@NotBlank(message="信箱不可為空白")
	private String email;
	@NotBlank(message="手機不可為空白")
	private String cellphone;
	private List<AgriculturalVo> shops;
//	private String shops;
}

