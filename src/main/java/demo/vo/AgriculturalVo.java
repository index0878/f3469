package demo.vo;

import lombok.Data;

@Data
public class AgriculturalVo{
	private String code;
	private int count;
	
}
