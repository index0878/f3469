package demo.vo;

import demo.enums.ResultEnum;
import lombok.Data;

@Data
public class ResultVo {

	private Integer code;
	private String message;

	public ResultVo(){
	    }

	public ResultVo(ResultEnum resultEnum){
	        this.code = resultEnum.getCode();
	        this.message = resultEnum.getMessage();
	    }

	public ResultVo(Integer code, String msg) {
	        this.code = code;
	        this.message = msg;
	    }

}
