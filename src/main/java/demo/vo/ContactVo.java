package demo.vo;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ContactVo {
	@NotBlank(message="姓名不可為空白")
	private String name;
	@NotBlank(message="信箱不可為空白")
	private String email;
	private String userType;
	@NotBlank(message="問題類型不可為空白")
	private String questionType;
	@NotBlank(message="留言不可為空白")
	private String message;
}
