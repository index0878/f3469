package demo.vo;

import lombok.Data;

@Data
public class ECPayVo {
	private String merchantID;
	private String merchantTradeNo;
	private String storeID;
	private String rtnCode;//若回傳值為1時，為付款成功，其餘代碼皆為交易異常
	/*
	 * 付款成功後綠界後端(server端)回傳：RtnMsg=交易成功
	 * 綠界後端透過排程補送通知回傳：RtnMsg=paid
	 * 付款成功後綠界回傳到OrderResultURL(client端)：RtnMsg=Succeeded
	 */
	private String rtnMsg;
	private String tradeNo;
	private String tradeAmt;
	private String paymentDate;//格式為yyyy/MM/dd HH:mm:ss
	private String paymentType;
	private String paymentTypeChargeFee;
	private String tradeDate;
	/*
	 * 是否為模擬付款
	 * 0：代表此交易非模擬付款。
	 * 1：代表此交易為模擬付款，RtnCode也為1。並非是由消費者實際真的付款，所以綠界也不會撥款給廠商，請勿對該筆交易做出貨等動作，以避免損失。
	 */
	private String simulatePaid;
	private String customField1;
	private String customField2;
	private String customField3;
	private String customField4;
	private String checkMacValue;
}
