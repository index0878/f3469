package demo.vo;

import java.util.List;
import java.util.Map;

import demo.entity.BlogEntity;
import demo.entity.CategoryEntity;
import demo.entity.HerfEntity;
import demo.entity.ShopEntity;
import lombok.Data;

@Data
public class DemoHomeVo {
	
	private String name;
	private String text;
	private List<HerfEntity> homeLinks;
	private String logo;
	private BlogEntity blog;
	private List<CategoryEntity> categories;
	private List<AdVo> ads;
	private List<Map<String,String>> data;
	private List<Map<String,String>> recentArticles;
	private List<ShopEntity> shops;
}
