package demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.entity.HomeEntity;

@Repository
public interface HomeRepository extends JpaRepository<HomeEntity, Long>{
	public List<HomeEntity> findByUserId(Long userId);
}
