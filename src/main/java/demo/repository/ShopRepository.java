package demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.entity.ShopEntity;

@Repository
public interface ShopRepository extends JpaRepository<ShopEntity, Long>{
	public List<ShopEntity> findByUserId(long userid); 
	public ShopEntity findByCode(String code);
}
