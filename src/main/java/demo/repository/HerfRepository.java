package demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.entity.HerfEntity;

@Repository
public interface HerfRepository extends JpaRepository<HerfEntity, Long>{
	
}
