package demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.entity.DetailEntity;

@Repository
public interface DetailRepository extends JpaRepository<DetailEntity, Long>{
	public List<DetailEntity> findByOrderId(long orderid); 
}
