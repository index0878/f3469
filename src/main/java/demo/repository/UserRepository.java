package demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> { 
	public UserEntity findByUserAccount(String account);
}
