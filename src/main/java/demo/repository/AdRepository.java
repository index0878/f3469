package demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.entity.AdEntity;

@Repository
public interface AdRepository extends JpaRepository<AdEntity, Long>{
	public List<AdEntity> findByUserId(long userid); 
}
