package demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.entity.BlogEntity;

@Repository
public interface BlogRepository extends JpaRepository<BlogEntity, Long> {
	public List<BlogEntity> findByUserId(long userId);
	public List<BlogEntity> findByCategoryId(long userId);
}
