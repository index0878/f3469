package demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "home")
public class HomeEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//ORCAL用AUTO，MYSQL用IDENTITY
	@Column(name = "home_id")
	private long id;
	
	private long herfId;
	private long userId;
}
