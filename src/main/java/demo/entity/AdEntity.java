package demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "Ad")
public class AdEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//ORCAL用AUTO，MYSQL用IDENTITY
	@Column(name = "ad_id")
	private long id;
	
	private long herfId;
	private long userId;
	@Lob
	private byte[] AdBlob;
}
