package demo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import lombok.Data;
import utils.ImageUtils;

@Data
@Entity
@Table(name="shop")
public class ShopEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//ORCAL用AUTO，MYSQL用IDENTITY
	private Long id;
	private String name;
	private String text;
	private String code;
	private Integer price;
	private Long userId;
	@Lob
	private byte[] blob;
	
	public String getBlob() {
		return ImageUtils.convertBinImageToString(this.blob);
	}
}
