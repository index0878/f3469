package demo.entity;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "blog")
public class BlogEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//ORCAL用AUTO，MYSQL用IDENTITY
	@Column(name = "blog_id")
	private long id;
	
	private long userId;
	private String title;
	private String text;
	private Date creattime;
	private long categoryId;
}
