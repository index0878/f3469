package demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="contact")
public class ContactEntity {
	@Id
	@Column(name="contact_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)//ORCAL用AUTO，MYSQL用IDENTITY
	private long id;
	private String name;
	private String email;
	private String userType;
	private String questionType;
	private String message;
}
