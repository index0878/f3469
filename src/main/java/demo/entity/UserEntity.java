package demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name="user")
public class UserEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//ORCAL用AUTO，MYSQL用IDENTITY
	@Column(name = "user_id")
	private long id;
	@Column(unique=true )
	private String userAccount;
	@Lob
	private byte[] userBlob;
	@Column(name="user_name")
	private String name;
	@Column(name = "user_text")
	private String text;
	
	
	
}
