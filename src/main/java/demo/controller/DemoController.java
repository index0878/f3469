package demo.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import demo.entity.BlogEntity;
import demo.entity.OrderEntity;
import demo.entity.UserEntity;
import demo.service.DemoService;
import demo.vo.ContactVo;
import demo.vo.DemoHomeVo;
import demo.vo.ResultVo;
import demo.vo.ShopVo;
import ecpay.payment.integration.AllInOne;
import ecpay.payment.integration.domain.AioCheckOutALL;
import ecpay.payment.integration.ecpayOperator.EcpayFunction;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import utils.ImageUtils;

@Controller
public class DemoController {
	
	@Autowired
	DemoService service;
	
	private Logger log = Logger.getLogger(DemoController.class);
	
	@RequestMapping("/helloHtml")
	public ModelAndView helloHtml() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("helloHtml");
		
		List<Map<String,String>> users = new ArrayList<>(); 
		Map<String,String> user1 = new HashMap<>();
		user1.put("name", "makabaka");
		Map<String,String> user2 = new HashMap<>();
		user2.put("name", "usidisi");
		users.add(user1);
		users.add(user2);
		mv.addObject("users", users);
		return mv;
	}
	
	@RequestMapping("/helloAllUser")
	public ModelAndView helloAllUser() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("helloAllUser");
		
		mv.addObject("users", service.getAllUser());
		return mv;
	}
	
	
	@RequestMapping("/blog")
	public ModelAndView blog(@RequestParam( name = "account", defaultValue = "f3469") String account) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("blog");
		UserEntity user = service.getUserEntity(account);
		BlogEntity blog = service.getblog(user.getId());
		
		DemoHomeVo vo = new DemoHomeVo();
		vo.setAds(service.getAdVos(user.getId()));
		vo.setCategories(service.getCategoryEntity(user.getId()));
		vo.setBlog(blog);
		vo.setRecentArticles(service.getBlogTitleAndIndexByUser(user.getId()));
		mv.addObject("vo",vo);
		return mv;
	}
	
	@RequestMapping("/indexblog")
	public ModelAndView indexblog(@RequestParam( name = "account", defaultValue = "f3469") String account, @RequestParam(required=true, name = "blogid", defaultValue = "0") long blogid) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("blog");
		UserEntity user = service.getUserEntity(account);
		BlogEntity blog = service.getblogById(blogid);
		if(blog == null || blog.getUserId() != user.getId()) {
			return blog(account);
		}
		
		DemoHomeVo vo = new DemoHomeVo();
		vo.setAds(service.getAdVos(user.getId()));
		vo.setCategories(service.getCategoryEntity(user.getId()));
		vo.setBlog(blog);
		vo.setRecentArticles(service.getBlogTitleAndIndexByUser(user.getId()));
		mv.addObject("vo",vo);
		return mv;
	}
	
	@RequestMapping("/category")
	public ModelAndView category(@RequestParam(name = "account", defaultValue = "f3469") String account,
			@RequestParam(required = true, name = "categoryid", defaultValue = "0") long categoryid) {
		if(categoryid < 1) {
			return blog(account);
		}
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("category");
		UserEntity user = service.getUserEntity(account);
		List<Map<String,String>> data = service.getBlogTitleAndIndexByCategory(categoryid);
		
		DemoHomeVo vo = new DemoHomeVo();
		vo.setAds(service.getAdVos(user.getId()));
		vo.setCategories(service.getCategoryEntity(user.getId()));
		vo.setData(data);
		vo.setRecentArticles(service.getBlogTitleAndIndexByUser(user.getId()));
		mv.addObject("vo",vo);
		return mv;
	}
	
	@RequestMapping("/home")
	public ModelAndView home(@RequestParam( name = "account", defaultValue = "f3469") String account) {
		ModelAndView mv = new ModelAndView();
		log.info("account : "+account);
		mv.setViewName("home");
		
		UserEntity user = service.getUserEntity(account);
		DemoHomeVo vo = new DemoHomeVo();
		vo.setLogo(ImageUtils.convertBinImageToString(user.getUserBlob()));
		vo.setName(user.getName());
		vo.setHomeLinks(service.getUserHome(user.getId()));
		vo.setText(user.getText());

		mv.addObject("vo",vo);
		
		return mv;
	}
	
	@RequestMapping("/contact")
	public ModelAndView contact(@RequestParam(name = "account", defaultValue = "f3469") String account) {
		
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("contact");
		UserEntity user = service.getUserEntity(account);
		
		DemoHomeVo vo = new DemoHomeVo();
		vo.setAds(service.getAdVos(user.getId()));
		vo.setCategories(service.getCategoryEntity(user.getId()));
		vo.setRecentArticles(service.getBlogTitleAndIndexByUser(user.getId()));
		mv.addObject("vo",vo);
		return mv;
	}
	
	@RequestMapping("/contact/sendForm")
	@ResponseBody
	public ResultVo sendForm(@Valid ContactVo vo){
		log.info(vo.toString());
		service.saveContactForm(vo);
		service.sendMail(vo);
		return new ResultVo(1000,"我們已收到您的聯繫");
	}
	
	@RequestMapping("/shop")
	public ModelAndView shop(@RequestParam(name = "account", defaultValue = "f3469") String account) {
		
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("shop");
		UserEntity user = service.getUserEntity(account);
		
		DemoHomeVo vo = new DemoHomeVo();
		vo.setAds(service.getAdVos(user.getId()));
		vo.setCategories(service.getCategoryEntity(user.getId()));
		vo.setRecentArticles(service.getBlogTitleAndIndexByUser(user.getId()));
		vo.setShops(service.getShopByUserid(user.getId()));
		mv.addObject("vo",vo);
		return mv;
	}
	
	@RequestMapping("/success")
	public ModelAndView success(@RequestParam(name = "account", defaultValue = "f3469") String account) {
		log.info("------------------INSERT success------------------------");

		ModelAndView mv = new ModelAndView();
		mv.setViewName("success");
		UserEntity user = service.getUserEntity(account);
		
		DemoHomeVo vo = new DemoHomeVo();
		vo.setAds(service.getAdVos(user.getId()));
		vo.setCategories(service.getCategoryEntity(user.getId()));
		vo.setRecentArticles(service.getBlogTitleAndIndexByUser(user.getId()));
		mv.addObject("vo",vo);
		return mv;
	}
	@RequestMapping("/checkReturn")
	@ResponseBody
	public String checkReturn(HttpServletRequest req) {
		log.info("------------------INSERT checkReturn------------------------");
		
		if(service.checkMacValue(req)) return "1|OK";
		
		return "false";
		
	}
	
	@RequestMapping("/shop/buy")
	@ResponseBody
	public ResultVo buy(@Valid @RequestBody ShopVo vo,HttpServletRequest request) {
//		int total = 0;
		log.info("------------------------------------------");
		log.info(vo.toString());
//		String shopsNAme = "";
		for(int i = 0 ; i < vo.getShops().size() ; i++ ) {
			log.info(vo.getShops().get(i).toString());
		}
		log.info("------------------------------------------");
//		service.saveOrder(vo);
//		
		
		return new ResultVo(1000,"我們已收到您的訂單，將為您轉跳付款頁面");
	}
	
	@RequestMapping(value="/buyByECPay" , produces = MediaType.TEXT_HTML_VALUE)
	public ModelAndView buyByECPay(ShopVo vo,HttpServletRequest request) {
		int total = 0;
		log.info("------------------------------------------");
		log.info(vo.toString());
		String shopsNAme = "";
		for(int i = 0 ; i < vo.getShops().size() ; i++ ) {
			if(i > 0) {
				shopsNAme = shopsNAme+"、";

			}
			log.info(vo.getShops().get(i).toString());
			total = service.getTotal(vo.getShops());
			shopsNAme = shopsNAme+vo.getName();
		}
		log.info("------------------------------------------");
		OrderEntity order = service.saveOrder(vo);
		AllInOne allInOne = new AllInOne();
//		AioCheckOutApplePay aioCheckOutApplePay = new AioCheckOutApplePay();
		AioCheckOutALL aioCheckOutALL = new AioCheckOutALL();
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String date = sdf.format(now);
//		String ordeCrNo = new SimpleDateFormat("yyyyMMddHHmm").format(now);
		String uri = request.getScheme() + "://" +   // "http" + "://
	             request.getServerName() +       // "myhost"
//	             ":" + request.getServerPort() + // ":" + "8080"
	             request.getContextPath();       // "/ContextPath"
		log.info("------------------------------------------");
		log.info("RETURN URL:"+ uri);
		log.info("RETURN date:"+ date);
		log.info("------------------------------------------");

		
		//填入必要的資料
		aioCheckOutALL.setMerchantID("2000132");
		aioCheckOutALL.setMerchantTradeNo("f3469TestOrder"+order.getId());
		aioCheckOutALL.setMerchantTradeDate(date);
//		aioCheckOutALL.setTotalAmount("2");
		aioCheckOutALL.setTradeDesc("test Description:"+shopsNAme);
		aioCheckOutALL.setItemName("TestItem:F3469");
		aioCheckOutALL.setReturnURL(uri+"/checkReturn");
		aioCheckOutALL.setNeedExtraPaidInfo("Y");
		aioCheckOutALL.setOrderResultURL(uri+"/success");
		aioCheckOutALL.setInvoiceMark("N");
		aioCheckOutALL.setTotalAmount(total+"");
//		aioCheckOutALL.setChooseSubPayment();
		
		log.info("AioCheckOutALL.toString():"+aioCheckOutALL.toString());
		
//		InvoiceObj invoice = new InvoiceObj();
		
		//回傳form表單的資料
		String form = allInOne.aioCheckOut(aioCheckOutALL, null);
		
		String macValue = EcpayFunction.genCheckMacValue("5294y06JbISpM5x9", "v77hoKGq4kWxNNIS", aioCheckOutALL);

		order.setMacValue(macValue);
		service.saveOrder(order);
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("ecPay");
		
		mv.addObject("formtext", form);
		return mv;
		
	}
}
