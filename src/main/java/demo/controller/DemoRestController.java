package demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoRestController {
	@RequestMapping("/hello")
	public Map<String,String> hello() {
		System.out.println("hello");
		Map<String,String> map = new HashMap<>();
		map.put("HI", "hi");
		map.put("HELLO", "hello");
		return map;
	}
}
