package demo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.logging.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.entity.AdEntity;
import demo.entity.BlogEntity;
import demo.entity.CategoryEntity;
import demo.entity.ContactEntity;
import demo.entity.DetailEntity;
import demo.entity.HerfEntity;
import demo.entity.HomeEntity;
import demo.entity.OrderEntity;
import demo.entity.ShopEntity;
import demo.entity.UserEntity;
import demo.repository.AdRepository;
import demo.repository.BlogRepository;
import demo.repository.CategoryRepository;
import demo.repository.ContactRepository;
import demo.repository.DetailRepository;
import demo.repository.HerfRepository;
import demo.repository.HomeRepository;
import demo.repository.OrderRepository;
import demo.repository.ShopRepository;
import demo.repository.UserRepository;
import demo.vo.AdVo;
import demo.vo.AgriculturalVo;
import demo.vo.ContactVo;
import demo.vo.ShopVo;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import utils.ImageUtils;

@Service
public class DemoService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private HomeRepository homeRepository;
	@Autowired
	private HerfRepository herfRepository;
	@Autowired
	private BlogRepository blogRepository;
	@Autowired
	private AdRepository adRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private ContactRepository contactRepository;
	@Autowired
	private MailService mailService;
	@Autowired
	private ShopRepository shopRepository;
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private DetailRepository detailRepository;

	private Logger log = Logger.getLogger(DemoService.class);

	public List<UserEntity> getAllUser() {
		return userRepository.findAll();
	}

	public UserEntity getUserEntity(String account) {
		return userRepository.findByUserAccount(account);
	}

	public List<HerfEntity> getUserHome(long userId) {
		List<HomeEntity> homes = homeRepository.findByUserId(userId);
		List<HerfEntity> rs = new ArrayList<>();
		for (HomeEntity home : homes) {
			HerfEntity herf = herfRepository.findById(home.getHerfId()).get();
			rs.add(herf);
		}

		return rs;
	}

	public BlogEntity getblog(long userid) {
		List<BlogEntity> list = blogRepository.findByUserId(userid);
		if (list.size() > 0) {
			return list.get(0);
		}
		return new BlogEntity();
	}

	public BlogEntity getblogById(long blogid) {
		BlogEntity blog = blogRepository.findById(blogid).get();

		return blog;
	}

	public List<CategoryEntity> getCategoryEntity(long userid) {
		return categoryRepository.findByUserId(userid);
	}

	public List<BlogEntity> getBlogsByCategory(long categoryid) {
		return blogRepository.findByCategoryId(categoryid);
	}

	public List<AdEntity> getAd(long userid) {
		return adRepository.findByUserId(userid);
	}

	public List<AdVo> changToAdvo(List<AdEntity> ads) {
		List<AdVo> rs = new ArrayList<>();
		for (AdEntity ad : ads) {
			AdVo vo = new AdVo();
			vo.setBlob(ImageUtils.convertBinImageToString(ad.getAdBlob()));
			HerfEntity herf = herfRepository.findById(ad.getHerfId()).get();
			vo.setLink(herf.getLink());
			vo.setTitle(herf.getTitle());
			rs.add(vo);
		}
		return rs;
	}

	public List<AdVo> getAdVos(long userid) {
		List<AdEntity> list = getAd(userid);
		return changToAdvo(list);
	}

	public List<Map<String, String>> getBlogTitleAndIndexByCategory(long categoryid) {
		List<Map<String, String>> rs = new ArrayList<>();
		List<BlogEntity> blogs = getBlogsByCategory(categoryid);
		for (BlogEntity blog : blogs) {
			Map<String, String> map = new HashMap<>();
			map.put("title", blog.getTitle());
			map.put("index", blog.getId() + "");
			rs.add(map);
		}

		return rs;
	}

	public List<Map<String, String>> getBlogTitleAndIndexByUser(long userid) {
		List<Map<String, String>> rs = new ArrayList<>();
		List<BlogEntity> blogs = blogRepository.findByUserId(userid);
		for (BlogEntity blog : blogs) {
			Map<String, String> map = new HashMap<>();
			map.put("title", blog.getTitle());
			map.put("index", blog.getId() + "");
			rs.add(map);
		}

		return rs;
	}

	public void saveContactForm(ContactVo vo) {
		ContactEntity entity = new ContactEntity();
		BeanUtils.copyProperties(vo, entity);
		contactRepository.save(entity);
	}

	public void sendMail(ContactVo vo) {
		mailService.sendSimpleMail(vo.getEmail(), "感謝您聯絡我們-F3469", vo.toString());
	}

	public void sendMail(String email, String subject, String text) {
		mailService.sendSimpleMail(email, subject, text);
	}

	public List<ShopEntity> getShopByUserid(long userid) {
		return shopRepository.findByUserId(userid);
	}

	@Transactional
	public OrderEntity saveOrder(ShopVo vo) {
		OrderEntity order = new OrderEntity();
		order.setName(vo.getEmail());
		order.setAddress(vo.getAddress());
		order.setEmail(vo.getEmail());
		order.setCellphone(vo.getCellphone());
		order.setStatus("N");
		order.setBuyDate(new Date());
		order = orderRepository.save(order);
		List<AgriculturalVo> list = vo.getShops();
		for (AgriculturalVo ag : list) {
			if (ag.getCount() < 1)
				continue;
			DetailEntity detail = new DetailEntity();
			detail.setOrderId(order.getId());
			detail.setShopCode(ag.getCode());
			detail.setCount(ag.getCount());
			detailRepository.save(detail);
		}

		return order;
	}

	public int getTotal(List<AgriculturalVo> list) {
		int rs = 0;
		for (AgriculturalVo ag : list) {
			ShopEntity shop = shopRepository.findByCode(ag.getCode());
			rs = rs + shop.getPrice() * ag.getCount();
		}
		return rs;
	}

	public boolean checkMacValue(HttpServletRequest req) {
		String orderId = req.getParameter("MerchantTradeNo");
		orderId = orderId.replaceAll("f3469TestOrder", "");
		OrderEntity order = orderRepository.findById(Long.valueOf(orderId)).get();
		// 檢查CheckMacValue
		String macValue = req.getParameter("CheckMacValue");
		String value = order.getMacValue();

		log.info("------------------------------------------");
		log.info("--------------------session macValue----------------------" + value);
		log.info("--------------------request macValue----------------------" + macValue);
		log.info("------------------------------------------" + req.getParameter("RtnCode"));
		if (macValue.equals(value) && "1".equals(req.getParameter("RtnCode"))) {
			sendMail(order.getEmail(), "感謝購買", "感謝" + order.getName() + "的購買");
		}else if( !"1".equals(req.getParameter("RtnCode"))){
			sendMail(order.getEmail(), "購買失敗", "非常抱歉，" + order.getName() + "，您的交易顯示失敗，請聯繫客服0987654321");
		}

		return macValue.equals(value);
	}

	public void saveOrder(OrderEntity order) {
		orderRepository.save(order);
		
	}

}
